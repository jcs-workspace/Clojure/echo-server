# echo-server

跟這個教程, [Clojure 从零到实战系列 - Clojure Web开发](https://www.bilibili.com/video/BV1qy4y1B7HP/).

## Prerequisites

You will need [Leiningen][] 2.0.0 or above installed.

[leiningen]: https://github.com/technomancy/leiningen

## Running

To start a web server for the application, run:

    lein ring server

## License

Copyright © 2023 FIXME
