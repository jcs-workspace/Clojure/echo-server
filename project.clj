(defproject echo-server "0.1.0-SNAPSHOT"
  :description "My first clojure project."
  :url "https://gitlab.com/jcs-workspace/Clojure/echo-server"
  :min-lein-version "2.0.0"
  :dependencies [[org.clojure/clojure "1.10.0"]
                 [compojure "1.6.1"]
                 [ring/ring-defaults "0.3.2"]
                 [http-kit "2.5.0"]]
  :main echo-server.handler
  :plugins [[lein-ring "0.12.5"]]
  :ring {:handler echo-server.handler/app}
  :profiles
  {:dev {:dependencies [[javax.servlet/servlet-api "2.5"]
                        [ring/ring-mock "0.3.2"]]}})
