@echo off
:: ========================================================================
:: $File: run.bat $
:: $Date: 2023-08-05 21:54:26 $
:: $Revision: $
:: $Creator: Jen-Chieh Shen $
:: $Notice: See LICENSE.txt for modification and distribution information
::                   Copyright © 2023 by Shen, Jen-Chieh $
:: ========================================================================

cd ../

lein run
